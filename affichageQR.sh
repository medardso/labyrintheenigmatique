#!/bin/bash

nb=$((1 + RANDOM %11))

echo "Répondez à la question suivante:"
ligne=$(head -n $nb QR.txt | tail -n 1) 

question=$(echo "$ligne" | cut -d ";" -f1)
echo $question

choix1=$(echo "$ligne" | cut -d ";" -f2)
choix2=$(echo "$ligne" | cut -d ";" -f3)
choix3=$(echo "$ligne" | cut -d ";" -f4)
echo $choix1
echo $choix2
echo $choix3

echo "Entrez votre choix"
read reponse

if [ "$reponse" != "a" ] && [ "$reponse" != "b" ] && [ "$reponse" != "c" ]
then 
	echo "Entrez a, b ou c !"
fi

correcte=$(echo "$ligne" | cut -d ";" -f5)
echo $correcte

case $reponse in
  $correcte) echo "c'est correct! vous pouvez continuer le chemin";;
  *) echo "Your bad, c'est pas ça, le chemin est bloqué, trouvez un autre chemin"
esac



