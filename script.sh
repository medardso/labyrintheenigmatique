 #! /bin/bash 
source CodeQuestions
function menuDemarrer {
	clear 
	echo "Jeu du labyrinthe "
	rep=j
	reponse=1
	while [ $rep != 'O' ] && [ $rep != 'N' ]
	do
		echo "Voulez-vous vraiment jouer ?(O/N)"
		read -n 1 rep
		echo ''
		if [ $rep = 'O' ]
		 then 
			echo "Vous avez choisi de jouer"
			genererMatrice
			while [ true ]
			do	
				LabMat
				checkpoints
				echo $reponse
				if [ $reponse = 1 ] 
				then
					FichIntoTab
					matrice[$murX,$murY]= "§" 
					reponse=0
				fi
				seDeplacer
				if [ $posX -eq 15 ] && [ $posY -eq 77 ]
				then 
					clear
					echo "YOU WIN !"
				fi	
				clear
			done
		else
			echo "Oh non ... Au revoir ;("
			sleep 1
		fi
	done 
}
function FichIntoTab {
		murX=$(grep "^$posX $posY " checkpoints_2  | cut -d " " -f3)
		murY=$(grep "^$posX $posY " checkpoints_2  | cut -d " " -f4)
}
function genererMatrice {
		for ((p=1;p<=taille;p++));
		do 
			  car=$(cat lab_complet | sed 's/ /f/g')
			  car=$(echo $car |  sed 's/ //g' | cut -c $p)
			  x=$( expr $p % $L )
			  y=$(expr $p / $L )
			  matrice[$x,$y]=$car
		done
}

function LabMat {
	a='*'
	for j in $(seq 0 $h)
	do
	   	for i in $(seq 0 $L)
		do
			#echo "$i $posX $j $posY"
			if [ $i -eq $posX ]  && [ $j -eq $posY ]
		 	then
		 		echo -n "*"
			 elif [ "${matrice[$i,$j]}" = 'f' ]
			 then
			
			 	echo -n ' '
			else
				echo -n ${matrice[$i,$j]}
			fi		
		done
		echo ''
	done	
}

function seDeplacer {

	read -n  1 fleche 
	case $fleche in 
	'z') if [ "${matrice[$posX,$(expr $posY - 1)]}" = 'f' ] 
		then
			echo $posY     
			posY=$(expr $posY - 1)
			echo $posY 
		else 
			echo $posY $posX
			echo ${matrice[$posX,$(expr $posY - 1)]}
			echo Nope
		fi ;;
	's') if [[ ("${matrice[$posX,$(expr $posY + 1)]}" = 'f' || "${matrice[$posX,$(expr $posY + 1)]}" = '_' )  && "${matrice[$posX,$posY]}" != '_' ]]
		then
			echo $posY     
			posY=$(expr $posY + 1)
			echo $posY 
		else 
			echo $posY $posX
			echo ${matrice[$posX,$(expr $posY + 1)]}
			echo Nope
		fi ;;
	'd') if [ "${matrice[$(expr $posX + 1),$posY]}" = 'f' ] || [ "${matrice[$(expr $posX + 1),$posY]}" = '_' ]
		then
			echo $posX     
			posX=$(expr $posX + 1)
			echo $posX
		else 
			echo $posY $posX
			echo ${matrice[$( expr $posX + 1), $posY )]}
			echo Nope	
		fi;;
	'q') if [ "${matrice[$(expr $posX - 1),$posY]}" = 'f' ]  || [ "${matrice[$(expr $posX - 1),$posY]}" = '_' ]
		 then
			echo $posX     
			posX=$(expr $posX - 1)
			echo $posX
		else 
			echo $posY $posX
			echo ${matrice[$( expr $posX - 1), $posY )]}
			echo Nope	
		fi;;
	  *) echo "Mauvaise touche..."
	esac
} 

function checkpoints {
 	echo "$posX $posY"
	echo $(grep "^$posY $posX " checkpoints_2)
	if [ $(grep -c "^$posY $posX " checkpoints_2) -eq 1 ] 
	then
		AffichageQuestions
		reponse=$?
	fi
}


#Programme principal (appel des fonctions)

#Variables 
#il faudra modifier la hauteur et la largeur
NbCheckP= 6 
posX=1
posY=1
h=19
L=84
taille=h*L
declare -A matrice 
declare -A tableau
menuDemarrer

